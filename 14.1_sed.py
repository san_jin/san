import os
def sed(pattern, replacement, file1,file2):
    '''
    read the first file and write the content into the second file
    if the pattern string appears in the file, use replacement string to replace it
    :param pattern:
    :param replacement:
    :param file1:
    :param file2:
    :return:
    '''
    fp= open(file1,'r')#read file1
    fout=open(file2,'w')#create file2
    for line in fp:
        if pattern in line:
            line=line.replace(pattern, replacement)
        fout.write(line)
    fp.close()
    fout.close()
def main():
    try:
        pattern='pattern'
        repalcement='repace'
        file1='sed_tester.txt'
        file2=file1+'.replaced'
        sed(pattern,repalcement,file1,file2)
    except Exception as e:
        print('Error message:',e)
if __name__ == '__main__':
    main()